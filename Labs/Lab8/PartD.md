[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)

## Part D - Test Multiple Pub/Sub Connections
**Synopsis:** In this part, we will test multiple in class nodes against the
MQTT gateway device.  We will also set up several gate way device and cross test.

## Objectives
* Assign hostnames for each gateway device
* Ensure Mosquitto is started and running
* Ensure MQTT.fx is started and attached/connected to the Mosquitto server.
* Ensure Publish and observe both MQTT.fx subscriptions, listener and mosquitto_pub

### Step D1: Assign Hostnames
```sh

```    
### Step D2: Ensure Mosquitto is started and running
```sh
pi$ ps aux | grep mosq |
```    
### Step D3: Ensure MQTT.fx is connected to Mosquitto


### Step D4: Publish and Subscribe!


[PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartC.md) Create MQTT Python Test Code

[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)
