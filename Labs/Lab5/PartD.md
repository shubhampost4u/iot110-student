[LAB5 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/setup.md)

## Part D - Add MorrisJS Charting Capability for the Pressure

**Synopsis:** In parts A-C we built up quite a capability from capturing our sensor
data in Python, converting the sensor data object to JavaScript and then a fairly
major overhaul of our webapp UI to get into position for adding multiple sensor
data types.  This is just the beginning of the typical process of creating a 
sensor dashboard.  For this last part we'll just detail what we had to do to 
successfully integrate the MorrisJS temperature chart and we'll leave it as the
Lab5 homework exercise to do the same for the pressure chart.

## Objectives

* Describe how to Integrate MorrisJS Charting Capability
* JavaScript code modifications and new functions to handle temperature charting
* Description of how to integrate pressure

### Step D1: MorrisJS Charting Capability

Morris.js provides a very simple way to get started with clean and useful chart
integration into our web displays including hover data point viewing.  Visit the 
following website for Morris to study the APIs.
[MorrisJS](http://morrisjs.github.io/morris.js/)

We will integrate the simple line chart based on their starter example.  First we 
will need to put an HTML element that will place the chart.  We will locate this 
in one of the jQuery Tabbed panel elements as follows in the ```<div id="tabs-temp">```
element:

```html
    <div id="tabs">
      <ul>
        <li><a href="#tabs-table-data">Table Data</a></li>
        <li><a href="#tabs-temp">Temperature Chart</a></li>
        <li><a href="#tabs-press">Pressure Chart</a></li>
      </ul>
      
      <div id="tabs-table-data">
        <table class="table table-hover table-striped">
            ... (table data)
        </table>
      </div>
      
      <div id="tabs-temp">
        <div id="mytempchart" style="height: 70%; width: 95%;"></div>
      </div>
      
      <div id="tabs-press">
        ...
      </div>
    </div>

```

The inclusion of MorrisJS charting will reference the css id ```mytempchart``` as
a means to render the chart.

### Step D2: JavaScript functions for Temperature Charting

We added Morris charting capability to ```lab5.js``` as follows.  First we need
to create an object to refer to the temperature chart. This contains a data
array that will hold a series of (time, y-value) to be plotted.  Once we've
instantiated the ```graph``` object, we can add data to it as shown in the next
code snippet.

```javascript
var graph = new Morris.Line({
  element: 'mytempchart',
  data: [],
  xkey: 'time',
  ykeys: ['value'],
  labels: ['Value']
});
```

In this simple dashboard, we have hardcoded 5 data points that stream update.  
That is, when we pass data to this chart it is the last 5 data points captured
on a moving time window of 5 data points.

```javascript
function update_temp_chart(data) {
  var chart_data = [
    { time: data[0]['time'], value: data[0]['temp'] },
    { time: data[1]['time'], value: data[1]['temp'] },
    { time: data[2]['time'], value: data[2]['temp'] },
    { time: data[3]['time'], value: data[3]['temp'] },
    { time: data[4]['time'], value: data[4]['temp'] }
  ];
  graph.setData(chart_data);
}
```

Finally, we'll show the ```updateSensors``` function which calls this function to
update the display.  Note that only whene the tab is selected will the chart be
in view on the web page.

```javascript
MBAR_TO_inHG = 0.029529983071;

updateSensors = (function (d) {
  var t_c = d['temperature'].reading
  var p_mbar = d['pressure'].reading
  var t_f = (t_c * 9) / 5.0 + 32.0;
  var p_inHg = p_mbar * MBAR_TO_inHG;

  var timedata = getDateNow();
  var t =  t_c.toFixed(1) + ' | ' + t_f.toFixed(1);
  var p =  p_mbar.toFixed(1) + ' | ' + p_inHg.toFixed(2);

  var obj = {};
  obj['date'] = timedata.date;
  obj['time'] = timedata.date;
  obj['temp'] = t;
  obj['press'] = p;
  data.push(obj);

  console.log(timedata);
  if (data.length > 5) {
    data.shift();
    clearTable();
    updateTable(data);
    update_temp_chart(data);
  }
});
```

The result of all these modifications is that we now have a temperature chart in
our dashbard on the temperature tab panel.
![TemperatureDashboardChart](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/DashboardTemperature.png)

### Step D3: Homework => Create MorrisJS Pressure Chart

For this week's homework you will want to make changes to the index.html (tab 
panel structure) and the JavaScript to incorporate a 5 datapoint pressure chart
much the same way the temperature chart was created.


[PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/PartC.md) Display Time, Temp and Pressure in a Bootstrap Table Structure

[LAB5 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/setup.md)
