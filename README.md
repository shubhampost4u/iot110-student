# IOT 110A/B - Internet of Things: Foundations Student Resource Repository #

## Link to Adobe Connect Online 
[Online Meeting Room](https://washington.zoom.us/my/things)

## Setting up RPi3
[RPi3 Setup](https://gitlab.com/iot110/iot110-student/blob/master/Resources/PI_SETUP.md)

## Outline
* [Lab 1](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab1/setup.md) - Flask WebApp - Hello IoT World!
* [Lab 2](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md) - LEDs and Switches (GPIO) via Python
* [Lab 3](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md) - Server Sent Events and Bootstrap UI
* [Lab 4](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab4/setup.md) - Sensors: I2C Pressure/Temperature via the BMP280
* [Lab 5](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab5/setup.md) - Sensors: Thing Data Protocols/Webapp for the BMP280
* [Lab 6](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab6/setup.md) - Sensors: Thing Data Protocols/Webapp for the SenseHat
* [Lab 7](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab7/setup.md) - Actuators: PWM LED Dimmer Controller & Stepper Motor / via the TB6612
* [Lab 8](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md) - Basic Networking (Simple Gateway Edge Devices)
* [Lab 9](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab9/setup.md) - Collect to Cloud using MQTT

## Webpage
[UW-PCE IoT Website](https://www.pce.uw.edu/certificates/internet-of-things)


## Lab Equipment List
| ITEM #  | DESCRIPTION |  QTY  | COST | COMPONENT | BUY URL| OTHER |
| :-----: | :---------- | :---: | ---: | :--------: | :-----------------| :-------- |
| 1  | Raspberry Pi 3 Kit | 1 | $89.95 | [RaspberryPi3](https://www.raspberrypi.org/) | [Canakit](https://www.amazon.com/dp/B01C6Q4GLE?psc=1) | [Alternate](https://www.adafruit.com/products/3058)|      
| 2  | Pi Sensor HAT    | 1 | $39.95 | Sensors | [Adafruit](https://www.adafruit.com/products/2738) | [Astro-Pi](https://astro-pi.org/) |
| 3  | Barometric Pressure & Temperature Sensor | 1 | $9.95 | [BMP280](https://www.bosch-sensortec.com/bst/products/all_products/bmp280) | [Adafruit](https://www.adafruit.com/products/2651) | [AppNote](https://cdn-learn.adafruit.com/downloads/pdf/adafruit-bmp280-barometric-pressure-plus-temperature-sensor-breakout.pdf) | [Datasheet](https://cdn-shop.adafruit.com/datasheets/BST-BMP280-DS001-11.pdf) |
| 4  | Stepper Motor Breakout Board | 1 | $4.95 | [TB6612](http://toshiba.semicon-storage.com/ap-en/product/linear/motordriver/detail.TB6612FNG.html) | [Adafruit](https://www.adafruit.com/products/2448) | [AppNote](https://cdn-learn.adafruit.com/downloads/pdf/adafruit-tb6612-h-bridge-dc-stepper-motor-driver-breakout.pdf) | [Datasheet](http://toshiba.semicon-storage.com/ap-en/product/linear/motordriver/detail.TB6612FNG.html) |
| 5  | Automotive Gauge Stepper Motor | 1| $9.95| | [AutoNeedle](https://www.adafruit.com/products/2424) | |
| 6  | TFT 5 inch Monitor |	1 | $13.32 | | [Adafruit](https://www.adafruit.com/products/2232) | | |
| 7  | HDMI Flat Cable | 1 | $11.29 | | [Adafruit](https://www.adafruit.com/products/2197) | |
| 8  | Microsoft Keyboard | 1 | $59.95 | | [Amazon](http://amzn.to/2djI91K) |
| 9 | Microsoft Wired Mouse | 1 | $3.95 | | [Amazon](http://amzn.to/2esKjlm) |
| 10 | Micro USB Charge Sync GOLD Data Cable | 2 |	$9.98 |	| [Amazon](http://amzn.to/2e49OHk) |
| 11 | Anker 24W Dual USB Wall Charger PowerPort 2 | 1 | $12.49 | | [Amazon](http://amzn.to/2ejevvC) |
| 12 | HEX STANDOFF 4-40 NYLON 1/2" | 8 | $5.60 | | [Digikey](http://bit.ly/2iWFqOT) |
| 13 | MACHINE SCREW PAN PHILLIPS 4-40 | 8 | $0.80 | | [Digikey](http://bit.ly/2iqhxlT) |
| 14 | Hookup Wire Kit |1 |13.49 | |[TEKTRUM 350](https://www.amazon.com/gp/product/B016AA75HY/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1) |
|    | | TOTAL | $285.62 | | |
